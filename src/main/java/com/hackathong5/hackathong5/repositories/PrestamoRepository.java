package com.hackathong5.hackathong5.repositories;

import com.hackathong5.hackathong5.hackathong5Application;
import com.hackathong5.hackathong5.models.PrestamoModel;
import com.hackathong5.hackathong5.services.PrestamoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PrestamoRepository {

    public List<PrestamoModel> findAll() {
        System.out.println("findAll en PrestamoRepository");

        return hackathong5Application.prestamoModels;
    }

    public Optional<PrestamoModel> findByIdUsuario(String idUsuario) {
        System.out.println("findByIdUsuario en PrestamoRepository");

        Optional<PrestamoModel> result = Optional.empty();

        for (PrestamoModel prestamoInList : hackathong5Application.prestamoModels) {
            if (prestamoInList.getIdUsuario().equals(idUsuario)) {
                System.out.println("Usuario encontrado");
                result = Optional.of(prestamoInList);

            }
        }

        return result;
    }

    public PrestamoModel save(PrestamoModel prestamo) {
        System.out.println("Save en PrestamoRepository");

        hackathong5Application.prestamoModels.add(prestamo);

        return prestamo;
    }

    public void delete(PrestamoModel prestamo) {
        System.out.println("delete en PrestamoRepository");
        System.out.println("Borrando prestamo");

        hackathong5Application.prestamoModels.remove(prestamo);
    }

   public Optional<PrestamoModel> findByBody(String idUsuario, String idProducto) {
        System.out.println("findByBody en PrestamoRepository");

        Optional <PrestamoModel> result = Optional.empty();
        System.out.println(idUsuario + " " + idProducto);

        for (PrestamoModel prestamoInList : hackathong5Application.prestamoModels){
            System.out.println(idUsuario + " " + idProducto);
            System.out.println(prestamoInList.getIdUsuario() + " " + prestamoInList.getIdProducto());
            if (prestamoInList.getIdUsuario().equals(idUsuario) &&
                    prestamoInList.getIdProducto().equals(idProducto)) {
                System.out.println("Prestamo encontrado en Repositorio");
                result = Optional.of(prestamoInList);
            }
        }

        return result;
    }

    public boolean update(PrestamoModel prestamo, float importe) {
        System.out.println("update en PrestamoRepository");
        System.out.println("Actualizando prestamo");

        System.out.println(prestamo.getIdUsuario() + prestamo.getIdProducto() + prestamo.getImporte());

        boolean result = false;

        Optional<PrestamoModel> prestamoToUpdate = this.findByBody(prestamo.getIdUsuario(), prestamo.getIdProducto());

       if (prestamoToUpdate.isPresent() == true) {
            System.out.println("Prestamo para actualizar encontrado");

            PrestamoModel prestamoFromList = prestamoToUpdate.get();

            prestamoFromList.setImporte(importe);

            result = true;

        }
       return result;
    }

}
