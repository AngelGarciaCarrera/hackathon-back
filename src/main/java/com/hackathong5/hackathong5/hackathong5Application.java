package com.hackathong5.hackathong5;

import com.hackathong5.hackathong5.models.PrestamoModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class hackathong5Application {

	public static ArrayList<PrestamoModel> prestamoModels;

	public static void main(String[] args) {
		SpringApplication.run(hackathong5Application.class, args);

		hackathong5Application.prestamoModels = hackathong5Application.getTestData();
	}

	private static ArrayList<PrestamoModel> getTestData() {

		ArrayList<PrestamoModel> prestamoModels = new ArrayList<>();

		prestamoModels.add(
				new PrestamoModel(
						"01"
						, "Pepe Ruiz"
						, "11"
						, "Prestamo"
						, 10000
				)
		);
		prestamoModels.add(
				new PrestamoModel(
						"02"
						, "Juana Martín"
						, "22"
						, "Cuenta personal"
						, 20000
				)
		);
		prestamoModels.add(
				new PrestamoModel(
						"03"
						, "Luis Pérez"
						, "33"
						, "Seguro"
						, 30000
				)
		);

		return prestamoModels;

	}
}