package com.hackathong5.hackathong5.models;

public class PrestamoModel {

    private String idUsuario;
    private String nameUsuario;
    private String idProducto;
    private String descProducto;
    private float importe;

    public PrestamoModel() {

    }

    public PrestamoModel(String idUsuario, String nameUsuario, String idProducto, String descProducto, float importe) {
        this.idUsuario = idUsuario;
        this.nameUsuario = nameUsuario;
        this.idProducto = idProducto;
        this.descProducto = descProducto;
        this.importe = importe;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNameUsuario() {
        return nameUsuario;
    }

    public void setNameUsuario(String nameUsuario) {
        this.nameUsuario = nameUsuario;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescProducto() {
        return descProducto;
    }

    public void setDescProducto(String descProducto) {
        this.descProducto = descProducto;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }
}
