package com.hackathong5.hackathong5.controllers;

import com.hackathong5.hackathong5.models.PrestamoModel;
import com.hackathong5.hackathong5.services.PrestamoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})

@RestController
@RequestMapping("/hackathon")

public class PrestamoController {

    @Autowired
    PrestamoService prestamoService;

    @GetMapping("/prestamos")
    public ResponseEntity<List<PrestamoModel>> getPrestamos() {
        System.out.println("getPrestamos");

        return new ResponseEntity<>(
                this.prestamoService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/prestamos/{idUsuario}")
    public ResponseEntity<Object> getPrestamoByIdUsuario(@PathVariable String idUsuario) {
        System.out.println("getPrestamoByIdUsuario");
        System.out.println("La id del usuario a buscar es " + idUsuario);

        Optional<PrestamoModel> result = this.prestamoService.findByIdUsuario(idUsuario);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

    @PostMapping("/prestamos")
    public ResponseEntity<PrestamoModel> addPrestamo(@RequestBody PrestamoModel prestamo) {
        System.out.println("addPrestamo");
        System.out.println("La id del usuario a crear es " + prestamo.getIdUsuario());
        System.out.println("La id del producto a crear es " + prestamo.getIdUsuario());
        System.out.println("El importe del prestamo a crear es " + prestamo.getImporte());

        return new ResponseEntity<>(
                this.prestamoService.add(prestamo),
                HttpStatus.CREATED
        );
    }

    @DeleteMapping("/prestamos")
    public ResponseEntity<String> deletePrestamo(@RequestBody PrestamoModel prestamo){
        System.out.println("deletePrestamo");

        boolean deletePrestamo = this.prestamoService.delete(prestamo.getIdUsuario(),prestamo.getIdProducto());

        return new ResponseEntity<>(
                deletePrestamo ? "Prestamo borrado" : "Prestamo no encontrado",
                deletePrestamo ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/prestamos")
    public ResponseEntity<Object> updatePrestamo(@RequestBody PrestamoModel prestamo) {
        System.out.println("updateProduct");

        System.out.println("La id del usuario que se va a actualizar es " + prestamo.getIdUsuario());
        System.out.println("La id del producto que se va a actualizar es " + prestamo.getIdProducto());
        System.out.println("El precio del producto que se va a actualizar es " + prestamo.getImporte());

        boolean updatePrestamo = this.prestamoService.update(prestamo.getIdUsuario(),prestamo.getIdProducto(), prestamo.getImporte());

        return new ResponseEntity<>(
                updatePrestamo ? "Prestamo actualizado" : "Prestamo no encontrado",
                updatePrestamo ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}

